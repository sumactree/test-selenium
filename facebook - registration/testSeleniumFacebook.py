from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time
import os

# TEST W CHROME

# Utworzenie nowej sesji Chrome :
driver = webdriver.Chrome(".../chromedriver.exe")
driver.implicitly_wait(30)
driver.maximize_window()
driver.get("https://www.facebook.com")
driver.maximize_window()
driver.implicitly_wait(20)
driver.save_screenshot("Screenshots/Facebook.png")
driver.find_element_by_id("email").send_keys("Selenium")
driver.find_element_by_id("pass").send_keys("Python")
driver.find_element_by_id("loginbutton").click()
driver.save_screenshot("Screenshots/Facebook1.png")
driver.get("https://www.facebook.com")
# driver.find_element_by_id("pass").send_keys(Keys.RETURN)

select = Select(driver.find_element_by_name('birthday_month'))

for index in range(len(select.options)):
    select = Select(driver.find_element_by_name('birthday_month'))
    select.select_by_index(index)
driver.save_screenshot("Screenshots/Facebook2.png")
driver.quit()
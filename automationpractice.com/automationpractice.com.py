from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time
import os
from random import randint

# TEST W CHROME

# Utworzenie nowej sesji Chrome :
driver = webdriver.Chrome("../chromedriver.exe")
driver.implicitly_wait(30)
driver.maximize_window()

driver.get("http://automationpractice.com/index.php")
driver.maximize_window()
driver.implicitly_wait(20)

# TC 1 - Zakładanie konta na stronie http://automationpractice.com/index.php

# TC 1.1 - Zakładanie konta z poprawnymi danymi :

# 1
driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[1]/a').click();
# 2
mail = str(randint(0,9999)) + '@19jo-mail.com'
driver.find_element_by_xpath('//*[@id="email_create"]').send_keys(mail);
# 3
driver.find_element_by_xpath('//*[@id="SubmitCreate"]/span').click();
# 4
driver.find_element_by_xpath('//*[@id="id_gender2"]').click();
# 5
driver.find_element_by_xpath('//*[@id="customer_firstname"]').send_keys('Ola');
# 6
driver.find_element_by_xpath('//*[@id="customer_lastname"]').send_keys('Skoczeń');
# 7
driver.find_element_by_xpath('//*[@id="passwd"]').send_keys('1234567890');
# 8
select = Select(driver.find_element_by_xpath('//*[@id="days"]'))
select.select_by_value('12');

select = Select(driver.find_element_by_xpath('//*[@id="months"]'))
select.select_by_value('4');


select = Select(driver.find_element_by_xpath('//*[@id="years"]'))
select.select_by_value('1965');
# 9
driver.find_element_by_xpath('//*[@id="address1"]').send_keys('Leśna');
# 10
driver.find_element_by_xpath('//*[@id="city"]').send_keys('Warszawa');
# 11
select = Select(driver.find_element_by_xpath('//*[@id="id_state"]'))
select.select_by_visible_text('California');
# 12
driver.find_element_by_xpath('//*[@id="postcode"]').send_keys('32343');
# 13
select = Select(driver.find_element_by_xpath('//*[@id="id_country"]'))
select.select_by_visible_text('United States');
# 14
driver.find_element_by_xpath('//*[@id="phone_mobile"]').send_keys('353634663');
# 15
driver.find_element_by_xpath('//*[@id="alias"]').clear()
driver.find_element_by_xpath('//*[@id="alias"]').send_keys(mail);
driver.save_screenshot("screenshots/TC 1 - 1.1.1.png")
# Screenshot całej strony :
el = driver.find_element_by_tag_name('body')
el.screenshot("screenshots/TC 1 - 1.1.2.png")

# 16
driver.find_element_by_xpath('//*[@id="submitAccount"]/span').click();

# 17
# driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[2]/a').click();

# TC 2 -  Logowanie na stronie http://automationpractice.com/index.php

# TC 2.1 - Logowanie prawidłowym emailem i hasłem :

# # 1
# driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[1]/a').click();
# # 2
# driver.find_element_by_xpath('//*[@id="email"]').send_keys('leruv@11jo-mail.com');
# # 3
# driver.find_element_by_xpath('//*[@id="passwd"]').send_keys('123456789');
# # 4
# driver.find_element_by_xpath('//*[@id="SubmitLogin"]').click();
#
# Screenshot całej strony :
#
# el = driver.find_element_by_tag_name('body')
# el.screenshot("screenshots/TC 1 - 2.1.1.png")
time.sleep(5)

# TC 3 - Dokonanie zakupu w sklepie internetowym http://automationpractice.com/index.php

# TC 3.1 - Zakup produktu :

# 1
driver.find_element_by_xpath('//*[@id="block_top_menu"]/ul/li[1]/a').click();
# 2
driver.find_element_by_xpath('//*[@id="center_column"]/ul/li[1]/div/div[2]/h5/a').click();
# 3
driver.find_element_by_xpath('//*[@id="quantity_wanted"]').clear()
driver.find_element_by_xpath('//*[@id="quantity_wanted"]').send_keys('1');
# 4
select = Select(driver.find_element_by_xpath('//*[@id="group_1"]'))
select.select_by_visible_text('S');
# 5
driver.find_element_by_xpath('//*[@id="color_13"]').click();
# 6
driver.find_element_by_xpath('//*[@id="add_to_cart"]/button/span').click();
# 7
driver.find_element_by_xpath('//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a/span').click();
# 8
driver.find_element_by_xpath('//*[@id="center_column"]/p[2]/a[1]').click();
# 9
driver.find_element_by_xpath('//*[@id="center_column"]/form/p/button').click();
# 10
driver.find_element_by_xpath('//*[@id="cgv"]').click();
# 11
driver.find_element_by_xpath('//*[@id="form"]/p/button').click();
# 12
driver.find_element_by_xpath('//*[@id="HOOK_PAYMENT"]/div[1]/div/p/a').click();
# 13
driver.find_element_by_xpath('//*[@id="cart_navigation"]/button').click();
# 14
driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[2]/a').click();

# Screenshot całej strony :
el = driver.find_element_by_tag_name('body')
el.screenshot("screenshotsTC/1 - 3.1.1.png")


driver.quit()
